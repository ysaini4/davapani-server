
describe('UsersService Tests', function() {

  var usersService;

  beforeEach(function() {
    usersService = require('../../../../app/services/users/users-service');
  });

  describe('lookupUsers', function() {

    it('should be a function', function(done) {
      expect(usersService.lookupUsers).to.be.a('function');
      done();
    });

  });
});
