var appointmentsService_ = require('../services/appointments.service');
function AppointmentsController() {
}
function book_appointment(req, res, next) {
	appointmentsService_.book_appointment(req.body.data,function(status,output){
      res.status(status).json(output);
  });
}
function get_appointment(req, res, next) {
	appointmentsService_.get_appointment(req.body.data,function(status,output){
      res.status(status).json(output);
  });
}

AppointmentsController.prototype = {
	book_appointment:book_appointment,
	get_appointment:get_appointment

}
module.exports = new AppointmentsController();
