function LabsController() {
	this.labService_ = require('../services/labs.service');
  	var labApis = ['lablist','addlab','updatelab'];
  	for (var i = 0; i < labApis.length; i++) {
	    this[labApis[i]] =  function(req, res, next) {
	        this.labService_[labApis[i]](req.body.data,function(status,output){
	            res.status(status).json(output);
	        });
	      }
	}
}
LabsController.prototype = {
}
module.exports = new LabsController();
