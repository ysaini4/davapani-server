function LabTestController() {
	this.labtestService_ = require('../services/lab.tests.service');
  	var labTestApis = ['labtestlist','addlabtest','updatelabtest'];
  	for (var i = 0; i < labTestApis.length; i++) {
	    this[labTestApis[i]] = function(req, res, next) {
	        this.labtestService_[labTestApis[i]](req.body.data,function(status,output){
	            res.status(status).json(output);
	        });
	      }
	    
	}
}
LabTestController.prototype = {
}
module.exports = new LabTestController();
