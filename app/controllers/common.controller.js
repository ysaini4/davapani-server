function UsersController() {
	this.commonService_ = require('../services/common.service');
}
function available_cities(req, res, next) {
	this.commonService_.available_cities(req.body.data,function(status,output){
      res.status(status).json(output);
  });
}
function doctor_speciality(req, res, next) {
	this.commonService_.doctor_speciality(req.body.data,function(status,output){
      res.status(status).json(output);
  });
}
UsersController.prototype = {
	available_cities:available_cities,
	doctor_speciality:doctor_speciality

}
module.exports = new UsersController();
