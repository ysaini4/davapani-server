var userModel = require('../models/users.model');
var doctorspecialityModel = require('../models/doctorspeciality.model');

function CommonService(){

}
function responseHandler(err,output,callback,statusCode=200){
 	if (err){
		callback(500,err);
 	} else{
 		var out;
 		if(statusCode == 200){
			out = this.getDefaults(true);
 		}
		else{
			out = this.getDefaults(false);
		}
		out.data = output
		callback(statusCode,out);
 	}
}
function getDefaults(status,auth){
	var out = {};
	out.status = status
	if(auth)
		out.auth = auth
	out.app = 'WEB_APP'
	return out;
}
function available_cities(data,callback){
	userModel.find({"city":{$ne: "" }}).lean().distinct('city',function(err, output) {
	commonService.responseHandler(err,output,callback)
	});
}
function doctor_speciality(data,callback){
	doctorspecialityModel.find().distinct('speciality',function(err, output) {
	commonService.responseHandler(err,output,callback)
	});
}
CommonService.prototype = {
	responseHandler:responseHandler,
	available_cities:available_cities,
	doctor_speciality:doctor_speciality,
	getDefaults:getDefaults
}
var commonService = new CommonService();

module.exports = commonService;
