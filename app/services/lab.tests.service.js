var labTestModel = require('../models/lab.tests.model');
var commonService = require('./common.service');

function LabTestService() {
}
function labtestlist(data,callback) {
	labTestModel.find().lean().exec(function(err, output) {
		commonService.responseHandler(err,output,callback)
	});
}
function addlabtest(data,callback){
	var lab = new labTestModel(data);
	lab.save(function (err, output) {
		output = data.name+' lab test added successfully.';
    	commonService.responseHandler(err,output,callback);
  	});
}
function updatelabtest(data,callback){
	labTestModel.update({'_id':data._id},data.update_data,function(err,output){
		var msg;
		if(!err){
			if(output.nModified){
				msg = 'Lab test details updated successfully.';
			} else {
				msg = 'No changes made.';
			}
		}
		var output = msg;
    	commonService.responseHandler(err,output,callback);
	})

}
LabTestService.prototype = {
  labtestlist: labtestlist,
  addlabtest: addlabtest,
  updatelabtest: updatelabtest,
}
module.exports = new LabTestService();
