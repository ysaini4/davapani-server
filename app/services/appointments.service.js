var appointmentsModel = require('../models/appointments.model');
var commonService = require('./common.service');
function AppointmentsService(){
}
function book_appointment(data,callback) {
	var appointment = new appointmentsModel(data);
	appointment.save(function (err, output) {
		output = 'appointment booked successfully.';
    	commonService.responseHandler(err,output,callback);
  	});
}
function get_appointment(data,callback) {
	appointmentsModel.find({doctor_id:data.doctor_id}).lean().exec(function(err, output) {
	commonService.responseHandler(err,output,callback)
	});
}
AppointmentsService.prototype = {
	book_appointment:book_appointment,
	get_appointment:get_appointment

}
var AppointmentsService = new AppointmentsService();
module.exports = AppointmentsService;
