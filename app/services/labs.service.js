var labsModel = require('../models/labs.model');
var commonService = require('./common.service');

function LabsService() {
}
function lablist(data,callback) {
	labsModel.find().lean().exec(function(err, output) {
	commonService.responseHandler(err,output,callback)
	});
}
function addlab(data,callback){
	var lab = new labsModel(data);
	lab.save(function (err, output) {
		output = data.name+' lab added successfully.';
    	commonService.responseHandler(err,output,callback);
  	});
}
function updatelab(data,callback){
	labsModel.update({'_id':data._id},data.update_data,function(err,output){
		var msg;
		if(!err){
			if(output.nModified){
				msg = 'Lab details updated successfully.';
			} else {
				msg = 'No changes made.';
			}
		}
		var output = msg;
    	commonService.responseHandler(err,output,callback);
	})

}
LabsService.prototype = {
  lablist: lablist,
  addlab: addlab,
  updatelab: updatelab,
}
var labsService = new LabsService();

module.exports = labsService;
