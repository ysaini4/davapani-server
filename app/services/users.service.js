var commonService = require('./common.service');
var errorList = require('./error-list');
var jwt = require('jsonwebtoken');
var userModel = require('../models/users.model');
var mongoose = require('../config/dbconfig.js');
//var bcrypt = require('bcryptjs');
function UsersService() {
	this.md5 = require('md5');
}

function userlist(callback) {
	 userModel.find(function(err, detail) {
	 	callback(200,detail);
        });  
}
function login(data,callback){
	console.log(data,'dd');
	let condation = {'username':data.username,'password':this.md5(data.password)} 
	if(!data.type)
		condation.type = {$ne:'admin'}
	else
		condation.type = data.type
	userModel.findOne(condation).lean().exec(function(err, output) {
		if(!output){
			output = errorList.INVALID_CREDENTIALS;
			commonService.responseHandler(err,output,callback,401)
		} else {
			output.token = jwt.sign({ id: output._id }, 'yogy', {
	    	  expiresIn: 86400 // expires in 24 hours
	    	});
		commonService.responseHandler(err,output,callback)
		}
	});  
}
function adduser(data,callback){
	data.password = this.md5(data.password);
	var person = new userModel(data);
	person.save(function (err, output) {
		output = data.type+' added successfully.';
    	commonService.responseHandler(err,output,callback);
  	});
}
function updatedoctor(data,callback){
	//userModel.add({color: 'string', price: 'number' });
	userModel.update({'_id':data._id},data.update_data,function(err,output){
			//callback(200,err);
		var msg;
		if(!err){
			if(output.nModified){
				msg = 'Details updated successfully.';
			} else {
				msg = 'No changes made.';
			}
		}
		var output = msg;
    	commonService.responseHandler(err,output,callback);
	})

}
function update_doctor_hospital(data,callback){

	userModel.update({'_id':data.doctor_id},{  $push: {hospitals:[{hospital_id:data.hospital_id}]} },function(err,out){
		//callback(200,out);
	})
	userModel.find({'_id':data.doctor_id}).lean().exec().then(output=>{
		return output
	}).then(output=>{
		console.log(output[0].name,'ddf')
		let doctorData = {
			doctor_id:data.doctor_id,
			name:output[0].name,
			speciality:output[0].speciality
		}

	userModel.update({'_id':data.hospital_id},{  $push: {doctors:[doctorData]} },function(err,out){
		callback(200,out);
	})		
		
	});
/*	this.Person.update({'_id':data.doctor_id},{ hospitals: [{ name: 'Matt' }, { name: 'Sarah' }] },function(err,out){
		callback(200,out);
	})*/

}
function checkvaliduser(data,callback){
	var out = commonService.getDefaults();
	callback(200,out);

}
function hospital_list(data,callback){
	let searchData = {'type':'hospital'}
	if(data){
		if(data.speciality && data.speciality != '')
		searchData.doctors = {$elemMatch:{speciality:{$in:data.speciality}}}
		if(data.city && data.city != '')
			searchData.city = data.city
		if(data._id && data._id != '')
			searchData._id = data._id
	}
	//searchData.doctors.speciality = {$in:data.speciality}
	/*callback(200,searchData)
	return false
*/
	userModel.find(searchData).lean().exec(function(err, output) {
	commonService.responseHandler(err,output,callback)
	});
}
function doctor_list(data,callback){
	let searchData = {type:'doctor'};
	if(data){
		if(data.speciality && data.speciality != '')
			searchData.speciality = {$in:data.speciality}
		if(data.city && data.city != '')
			searchData.city = data.city
		if(data._id && data._id != '')
			searchData._id = data._id

	}
	userModel.find(searchData).lean().exec().then((output)=> {
	return output
	}).then((output)=>{
		hospitalIds = [];
		//userModel.find({'type':'doctor'}).lean().exec().then((output)=> {})
		for(key in output){
			for(hos in output[key].hospitals){
				//console.log(output.data[key].hospitals[hos].hospital_id)
				hospitalIds.push(output[key].hospitals[hos].hospital_id)
			}
		}
		commonService.responseHandler('',output,callback)

	}).catch((err)=>{
		commonService.responseHandler(err,'',500,callback)
	})
	/*then(function(output,err) {
		console.log(output,err)
		commonService.responseHandler(err,out,statusCode,callback)
		var status = (!output)?false:true;
		var statusCode = (!output)?400:200;
		var out = commonService.getDefaults(status);
		out.data = output
		
	});*/
}
UsersService.prototype = {
userlist:userlist,
login:login,
adduser:adduser,
updatedoctor:updatedoctor,
update_doctor_hospital:update_doctor_hospital,
hospital_list:hospital_list,
doctor_list:doctor_list
};

var usersService = new UsersService();

module.exports = usersService;
