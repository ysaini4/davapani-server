var settingsConfig = require('./settings/settings-config');
var commonService = require('../services/common.service');
var errorList = require('../services/error-list');
var base64 = require('base-64');
var mongoose = require('../config/dbconfig.js');
var jwt = require('jsonwebtoken');

var accessKeys = mongoose.model("access_keys",  mongoose.Schema({}),'access_keys');
function RouteConfig() {
}

function registerRoutes(application) {
  var config = loadRouteConfig();

  for(var i = 0, length = config.routes.length; i < length; i++) {
    var routeItem = config.routes[i];

    var controller = loadController(routeItem);
    var route = getRoute(routeItem);
    var method = getMethod(routeItem);
    var action = getAction(routeItem);
    var checkAuth = getCheckAuth(routeItem);

    registerRoute(application, controller, route, method, action,checkAuth);
  }

  createConfigRoute(application);
}

function loadRouteConfig() {
  var config;

  try {
    config = require('./route.config.json');

    if(!config.routes || config.routes.length === 0) {
      throw '"routes" not defined';
    }
  }
  catch(e) {
    throw 'Unable to parse "lib/config/route.config.json": ' + e;
  }

  return config;
}

function loadController(routeItem) {
  var controller;
  if(!routeItem || !routeItem.controller) {
    throw 'Undefined "controller" property in "lib/config/route.config.json"';
  }

  try {
    controller = require(routeItem.controller);
  }
  catch(e) {
    throw 'Unable to load ' + routeItem.controller + ": " + e;
  }

  return controller;
}

function getRoute(routeItem) {
  if(!routeItem || !routeItem.route || routeItem.route.length === 0) {
    throw 'Undefined or empty "route" property in "lib/config/route.config.json"';
  }

  return routeItem.route;
}

function getMethod(routeItem) {
  if(!routeItem || !routeItem.method || routeItem.method.length === 0) {
    throw 'Undefined or empty "method" property in "lib/config/route.config.json"';
  }

  var method = routeItem.method.toLowerCase();

  switch(method) {
    case 'get':
    case 'put':
    case 'post':
    case 'delete':
      return method;
      break;
    default:
      throw 'Invalid REST "method" property in "lib/config/route.config.json": ' + method;
  }
}

function getAction(routeItem) {
  if(!routeItem || !routeItem.action || routeItem.action.length === 0) {
    return getMethod(routeItem);
  }
  return routeItem.action;
}
function getCheckAuth(routeItem) {
  if(!routeItem || !routeItem.checkAuth || routeItem.checkAuth.length === 0) {
    return false;
  }
  return routeItem.checkAuth;
}


function registerRoute(application, controller, route, method, action,checkAuth) {
  application.route(route)[method]((checkAuth?checkHeader:function(req, res,next){
    next();
  }),function(req, res, next) {
    controller[action](req, res, next);
  });
}
var checkHeader= function(req,res,next){
    var header = req.headers.token;
    var userid = req.headers.userid;
    var out = commonService.getDefaults(false,req.headers.authorization);
     if (!header){
        out.message = errorList.NO_TOKEN_PROVIDED
        res.status(401).send(out);
        return false;
    }
    if (!userid){
        out.message = errorList.NO_USERID_PROVIDED
        res.status(401).send(out);
        return false;
    }
    jwt.verify(header, 'yogy', function(err, decoded) {
      
      if(decoded && decoded.id != userid){
        out.message = errorList.TOKEN_NOT_VALID
        res.status(401).send(out);
        return false;
      }

    if (err){
      out.message = errorList.FAILED_TO_AUTHENTICATE_TOKEN
      res.status(500).send(out);
    } else{
      next();
    }  
  });
    /*accessKeys.findOne({'user_id':base64.decode(header[1]),'access_token':header[0]}).lean().exec(function(err, output) {
      if(err || !output){
        var out = commonService.getDefaults(false,req.headers.authorization);
        out.message = errorList.ACCESS_DENIED
        res.status(401).json(out)
      } else {
        next();
      }
    })*/
}

function createConfigRoute(application) {
  application.route('/config').get(function(req, res, next) {
    res.status(200).json(settingsConfig.settings);
  });
}

RouteConfig.prototype = {
  registerRoutes: registerRoutes
};

var routeConfig = new RouteConfig();

module.exports = routeConfig;
