var mongoose = require('../config/dbconfig.js');
var childHospitals = new mongoose.Schema({ hospital_id: String });
var childDoctors = new mongoose.Schema({doctor_id: String,name: {first:String,last:String},speciality: [String]});
var Schema = new mongoose.Schema({
		ID : String,
	    username : String,
	    password : String,
	    mobile : String,
	    type : String,
	    speciality : [String],
	    address_line : String,
	    city : String,
	    state : String,
	    country : String,
	    name : {first:String,last:String},
	    email : String,
	    status : String,
	    about : String,
	    zip_code : String,
	    last_login : String,
		hospitals: [childHospitals],
		doctors: [childDoctors],
	    modification_time : String,
	    creation_time : String
	});
module.exports = mongoose.model("users", Schema,'users');
