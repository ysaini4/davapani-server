var mongoose = require('../config/dbconfig.js');
var Schema = new mongoose.Schema({
	    patient_name : String,
	    patient_mobile : String,
	    doctor_id : String,
	    doctor_name : String,
	    book_time : String,
	    modification_time : String,
	    creation_time : String
	});
module.exports = mongoose.model("appointments", Schema,'appointments');
