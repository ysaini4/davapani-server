var mongoose = require('../config/dbconfig.js');
var Schema = new mongoose.Schema({
	    name : String,
	    title : String,
	    about : String,
	    modification_time : String,
	    creation_time : String
	});
module.exports = mongoose.model("labs", Schema,'labs');
